
package entity;

import java.util.LinkedList;

public class Data {
    public int id;
    String name;
    int age;
     public LinkedList<Courses> listOne = new LinkedList<>();
    public Data(int id, String name, int age,LinkedList<Courses> listOne){
        this.id=id;
        this.name=name;
        this.age=age;
        this.listOne=listOne;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public LinkedList<Courses> getListOne() {
        return listOne;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setListOne(LinkedList<Courses> listOne) {
        this.listOne.addAll(listOne);
    }
    
    
}
