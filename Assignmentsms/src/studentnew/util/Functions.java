package util;

import entity.Courses;
import entity.Data;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;

public class Functions {

    LinkedList<Data> listOfStudents = new LinkedList<>();
    LinkedList<Courses> linkCourse = new LinkedList<>();
    int id, age, courses;
    String name, cName, cCode;
    Courses com;

    public void add() {
        LinkedList<Courses> linkCourseTemp = new LinkedList<>();
        System.out.print("Enter Student Name: ");
        Scanner obj = new Scanner(System.in);
        name = obj.nextLine();

        System.out.print("Enter Student Id: ");
              id = obj.nextInt();

        System.out.print("Enter Student Age: ");
        age = obj.nextInt();

        System.out.print("Enter Number of Courses: ");
          courses = obj.nextInt();

        for (int k = 0; k < courses; k++) {
             Courses com = new Courses();
            System.out.print("Enter Course Name: ");
            Scanner ob = new Scanner(System.in);
            com.cName = ob.nextLine();

            System.out.print("Enter Course Code: ");
            Scanner ob1 = new Scanner(System.in);
            com.cCode = ob1.nextLine();

            linkCourseTemp.add(com);
        }
        Data std = new Data(id, name, age, linkCourseTemp);
        listOfStudents.add(std);
    }

    public void display() {
        Iterator<Data> n = listOfStudents.iterator();
        while (n.hasNext()) {
            Data custTemp = n.next();
            System.out.println("Student Id: " + custTemp.getId());
            System.out.println("Student Name: " + custTemp.getName());
            System.out.println("Student Age: " + custTemp.getAge());

            Iterator<Courses> listOne = custTemp.listOne.iterator();
            
            while (listOne.hasNext()) {
                System.out.println(".....list of courses......");
                Courses courseTemp = listOne.next();
                System.out.println("Course Name: " + courseTemp.getcName());
                System.out.println("Course Code: " + courseTemp.getcCode());
            }

        }
    }

    public Data addNewCourse(LinkedList<Courses> listOne) {
        int idFound = 0;

        Data newRecord = null;
    
        try {
            System.out.println("Enter Student Id: ");
            Scanner nc = new Scanner(System.in);
            int newId = Integer.parseInt(nc.nextLine());
            Iterator<Data> itrStudent = listOfStudents.iterator();
            while (itrStudent.hasNext()) {
                newRecord = (Data) itrStudent.next();
                if (newRecord.id == newId) {
                    idFound = 1;
                    break;
                }
            }
            if (idFound == 1) {
                Courses course = new Courses();
                System.out.println("Enter Course name: ");
                Scanner b = new Scanner(System.in);
                course.cName = b.nextLine();
                System.out.println("Enter Course Code: ");
                Scanner c = new Scanner(System.in);
                course.cCode = c.nextLine();
                newRecord.listOne.add(course);
            } else {
                System.out.println("Id not found");
            }

        } catch (Exception e) {
            System.out.println(e+ "enter the input in correct format");
        }
        return newRecord;
    }

    public void search() {
        int m;
        Data cu;
        System.out.print("Enter Id to be searched: ");
        Scanner bj = new Scanner(System.in);
        m = bj.nextInt();
        Iterator<Data> list = listOfStudents.iterator();
        while (list.hasNext()) {
            cu = list.next();
            if (cu.getId() == m) {
                System.out.println("Name: " + cu.getName());
                return;
            }
        }
        System.out.println("Id not found ");
    }
}
