
package studentmain;

import entity.Courses;
import java.util.LinkedList;
import java.util.Scanner;
import static util.Conventions.add;
import static util.Conventions.addNewCourse;
import static util.Conventions.display;
import static util.Conventions.search;
import util.Functions;

public class Studentsms{
static LinkedList<Courses> course = new LinkedList<>();
    public static void main(String[] args) {
        Functions Function = new Functions();
        int choice,choice1;
        do{
        System.out.println("1.Add 2.Display all 3.Add new Course 4.Search student ");
        Scanner obj = new Scanner(System.in);
        choice = obj.nextInt();
        
        switch(choice){
            case add:
                Function.add();
                break;
            case display:
                Function.display();
                break;
            case addNewCourse:
                Function.addNewCourse(course);
                break;
            case search:
                Function.search();
                break;
            default:
                break;
        } 
        System.out.print("Press 0 to continue 1 to exit: ");
        Scanner obj1 = new Scanner(System.in);
        choice1 = obj1.nextInt();
        }while(choice1  == 0);
        
    }
    
}
